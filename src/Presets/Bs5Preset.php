<?php

namespace Smorken\Preset\Presets;

use Illuminate\Filesystem\Filesystem;

class Bs5Preset extends Base
{
    public static function install(): void
    {
        static::updatePackages();
        static::updateAssets();
        static::updateBootstrapping();
        static::removeNodeModules();
        static::copyViews();
    }

    protected static function copyViews(): void
    {
        tap(new Filesystem(), function ($filesystem) {
            self::createBackup($filesystem, [resource_path('views/layouts')], 'directory');
            self::doCopy($filesystem, [[self::getLocalPath('bootstrap5/resources/views'), resource_path('views')]],
                'directory');
        });
    }

    protected static function updateAssets(): void
    {
        tap(new Filesystem(), function ($filesystem) {
            $pub = ['js', 'css', 'fonts'];
            foreach ($pub as $item) {
                $filesystem->deleteDirectory(public_path($item));
                $filesystem->makeDirectory(public_path($item), 0755, true);
            }
            $bkup = [
                resource_path('sass'),
                resource_path('js'),
            ];
            self::createBackup($filesystem, $bkup, 'directory');
            $list = [
                [self::getLocalPath('bootstrap5/resources/sass'), resource_path('sass')],
                [self::getLocalPath('bootstrap5/resources/js'), resource_path('js')],
            ];
            self::doCopy($filesystem, $list, 'directory');
        });
    }

    protected static function updateBootstrapping(): void
    {
        tap(new Filesystem(), function ($filesystem) {
            $list = [
                [self::getLocalPath('bootstrap5/webpack.mix.js'), base_path('webpack.mix.js')],
                [self::getLocalPath('bootstrap5/package.json'), base_path('package.json')],
                [self::getLocalPath('bootstrap5/babel.config.json'), base_path('babel.config.json')],
                [self::getLocalPath('bootstrap5/jest.config.js'), base_path('jest.config.js')],
            ];
            self::doDeleteAndCopy($filesystem, $list, 'file');
        });
    }
}

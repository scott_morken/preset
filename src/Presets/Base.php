<?php

namespace Smorken\Preset\Presets;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Laravel\Ui\Presets\Preset;

class Base extends Preset
{
    protected static function createBackup(Filesystem $fs, iterable $list, string $type = 'file'): void
    {
        $type = $type !== 'file' ? 'Directory' : null;
        foreach ($list as $item) {
            $del = 'delete'.$type;
            $make = 'make'.$type;
            if ($fs->exists($item)) {
                self::doCopy($fs, [[$item, $item.'.bak']], $type);
                $fs->$del($item);
            }
            if ($type) {
                $fs->$make($item);
            }
        }
    }

    protected static function doCopy(Filesystem $fs, iterable $list, string $type = 'file'): void
    {
        $action = 'copy';
        if ($type !== 'file') {
            $action = 'copyDirectory';
        }
        foreach ($list as $item) {
            [$from, $to] = $item;
            if ($from && $to) {
                echo "Copying $from -> $to: ";
                $r = $fs->$action($from, $to);
                echo (bool) $r."\n";
            } else {
                throw new \InvalidArgumentException("Path [ $from => $to ] is invalid.");
            }
        }
    }

    protected static function doDelete(Filesystem $fs, iterable $list, string $type = 'file'): void
    {
        $action = 'delete';
        if ($type !== 'file') {
            $action = 'deleteDirectory';
        }
        foreach ($list as $item) {
            [$from, $to] = $item;
            echo "Deleting $to: ";
            $r = $fs->$action($to);
            echo (bool) $r."\n";
        }
    }

    protected static function doDeleteAndCopy(Filesystem $fs, iterable $list, string $type = 'file'): void
    {
        self::doDelete($fs, $list, $type);
        self::doCopy($fs, $list, $type);
    }

    protected static function getLocalPath(string $localname): string|false
    {
        return realpath(__DIR__.'/../../data/'.$localname);
    }

    protected static function updatePackageArray(array $packages): array
    {
        return array_merge([], Arr::except($packages, []));
    }
}

<?php

namespace Smorken\Preset\Presets;

use Illuminate\Filesystem\Filesystem;

class Rad extends Base
{
    public static function install(): void
    {
        self::delete();
        self::copy();
        self::setTimeZone();
    }

    protected static function copy(): void
    {
        tap(new Filesystem(), function ($filesystem) {
            self::doCopy($filesystem, [[self::getLocalPath('rad'), base_path()]], 'directory');
            $copyfiles = [
                [self::getLocalPath('rad/.dockerignore.copy'), base_path('.dockerignore')],
                [self::getLocalPath('rad/.gitignore.copy'), base_path('.gitignore')],
                [self::getLocalPath('rad/.env'), base_path('.env')],
                [self::getLocalPath('rad/.env'), base_path('.env.example')],
                [self::getLocalPath('rad/.env.dusk.local'), base_path('.env.dusk.local.example')],
                [self::getLocalPath('rad/.env.testing'), base_path('.env.testing.example')],
                [self::getLocalPath('rad/composer.json'), base_path('composer.json')],
                [self::getLocalPath('rad/permissions.sh'), base_path('permissions.sh.example')],
            ];
            self::doDeleteAndCopy($filesystem, $copyfiles, 'file');
        });
    }

    protected static function delete(): void
    {
        tap(new Filesystem(), function ($filesystem) {
            $deletedirs = [
                [null, database_path()],
                [null, app_path('Http/Controllers/Auth')],
            ];
            self::doDelete($filesystem, $deletedirs, 'directory');
            self::doDelete($filesystem, [[null, app_path('User.php')]]);
            self::doDelete($filesystem, [[null, app_path('Http/Controllers/Controller.php')]]);
            self::doDelete($filesystem, [[null, config_path('sanctum.php')]]);
        });
    }

    protected static function setTimeZone(): void
    {
        $data = file_get_contents(config_path('app.php'));
        $data = str_replace("'timezone' => 'UTC'", "'timezone' => 'America/Phoenix'", $data);
        file_put_contents(config_path('app.php'), $data);
    }
}

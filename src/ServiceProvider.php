<?php

namespace Smorken\Preset;

use Laravel\Ui\UiCommand;
use Smorken\Preset\Presets\Bs5Preset;
use Smorken\Preset\Presets\Rad;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        UiCommand::macro('bootstrap', function ($command) {
            Bs5Preset::install();
            $command->info('Bootstrap 5 installed successfully.');
            $command->info('Please run "npm install && npm run dev" to build your public assets.');
        });

        UiCommand::macro('rad', function ($command) {
            Rad::install();
            $command->info('RAD environment installed successfully.');
            $command->info('Run "composer update".');
        });
    }
}

# Laravel presets
__Based on my RAD deployment__ (aka, might not be useful for you...)

## Usage

1. Fresh install Laravel.
2. Install `composer require smorken/preset --dev`. 
Service Provider autodiscovery is enabled for this package.

### Install Presets

Install laravel
 * `composer create-project --prefer-dist laravel/laravel my_project`
 * `laravel new my_project`
1. `cd my_project`
2. `composer require smorken/preset --dev`
3. `php artisan preset bootstrap`
4. `php artisan preset rad`
5. `composer update`
6. `npm install && npm run dev`


### My Env
```shell script
larainstall new PROJECT
cd PROJECT
composer require smorken/preset smorken/docker --dev
cp -a vendor/smorken/docker/lemp/. docker.copy/
cd docker.copy
docker-compose up -d
make connect_php
php artisan preset bootstrap
php artisan preset rad
exit
make chown
make publish_dev
```

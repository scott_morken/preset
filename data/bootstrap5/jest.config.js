module.exports = {
  testEnvironment: "jsdom",
  roots: ["<rootDir>/resources/js/__tests__"],
  testMatch: ["**/*.test.js"],
  setupFilesAfterEnv:['<rootDir>/resources/js/__tests__/setup-jest.js']
};

const mix = require('laravel-mix');
const fs = require('fs');
require("laravel-mix-tailwind");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let getFiles = function (dir) {
    return fs.readdirSync(dir).filter(file => {
        return fs.statSync(`${dir}/${file}`).isFile();
    })
};

mix.setResourceRoot('../');
mix.setPublicPath('public');

getFiles(path.join(__dirname, 'resources/js/limited')).forEach(function (filepath) {
    if (filepath[0] !== '.') {
        mix.js('resources/js/limited/' + filepath, 'public/js/limited/' + filepath);
    }
});

mix.js('resources/js/app.js', 'public/js');
if (!process.env.NODE_JS_ONLY) {
    mix.sass('resources/sass/app.scss', 'public/css')
        .tailwind('./tailwind.config.js')
        .copy('resources/images', 'public/images');
}

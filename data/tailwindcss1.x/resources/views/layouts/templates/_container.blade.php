<div class="container">
    @includeIf('layouts.menus.submenu')
</div>
<main class="py-4 container">
    @yield('content')
</main>
<footer class="py-4 container">
    <div class="text-center">
        <div class="text-muted"><small>&copy; {{ date('Y') }} Phoenix College</small>
            @if (\Illuminate\Support\Facades\Session::has('impersonating'))
                <span class="text-danger">&middot; Impersonating [{{ \Illuminate\Support\Facades\Session::get('impersonating') }}]</span>
            @endif
            @if (file_exists(public_path('images/footer.png')))
                <div>
                    <a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges">
                        <img src="{{ asset('images/footer.png') }}" alt="A Maricopa Community College"
                             height="30">
                    </a>
                </div>
            @endif
        </div>
    </div>
    @yield('footer')
</footer>
@include('layouts._partials._modal')

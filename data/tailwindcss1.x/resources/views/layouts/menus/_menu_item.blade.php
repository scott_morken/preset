@if ($menu->visible)
    <li class="nav-item">
        <a class="nav-link {{ $active ? 'active' : null }}"
           href="{{ action($menu->action) }}">{{ $menu->name }}</a>
    </li>
@endif

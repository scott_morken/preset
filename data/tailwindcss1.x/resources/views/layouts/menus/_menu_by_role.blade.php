@can($role)
    @php $menus = \Smorken\Menu\Facades\Menu::getMenusByKey($role); @endphp
    @if ($menus)
        @php
            $submenu = null;
            $id = sprintf('navbar-%s-dropdown', \Illuminate\Support\Str::slug($role));
        @endphp
        <li class="nav-item dropdown">
            <a id="{{ $id }}"
               class="nav-link dropdown-toggle {{ \Smorken\Menu\Facades\Menu::isActiveChain($controller ?? null, $menus) ? 'active' : null }}"
               href="#" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false" v-pre>
                {{ $title ?? 'Admin' }} <span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="{{ $id }}">
                @foreach ($menus as $menu)
                    @php
                        $active = \Smorken\Menu\Facades\Menu::isActiveChain($controller ?? null, $menu);
                        $submenu = (is_null($submenu) && $active && count($menu->children)) ? $menu->children : $submenu;
                    @endphp
                    @if ($menu->visible)
                        <a class="dropdown-item {{ $active ? 'active' : null }}"
                           href="{{ action($menu->action) }}">{{ $menu->name }}</a>
                    @endif
                @endforeach
            </div>
        </li>
        @if ($submenu)
            @php \Smorken\Menu\Facades\Menu::setSubMenus($submenu); @endphp
        @endif
    @endif
@endcan

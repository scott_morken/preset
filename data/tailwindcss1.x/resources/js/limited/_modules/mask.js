let masked = {
    id: '#toggle-mask',
    class: {
        check: 'text-success fa-eye',
        uncheck: 'text-danger fa-eye-slash masked',
    },
    init() {
        $(masked.id).on('click', function (e) {
            masked.toggle();
        })
    },
    toggle() {
        let ele = $(this.id + ' i')[0];
        if (this.isMasked()) {
            this.unmask(ele);
        } else {
            this.mask(ele);
        }
    },
    mask(element) {
        $('.can-blur').addClass('blur');
        $(element).removeClass(this.class.check);
        $(element).addClass(this.class.uncheck);
    },
    unmask(element) {
        $('.can-blur').removeClass('blur');
        $(element).removeClass(this.class.uncheck);
        $(element).addClass(this.class.check);
    },
    isMasked() {
        let ele = $(this.id + ' i')[0];
        return $(ele).hasClass('masked');
    }
};

module.exports = masked;

const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    theme: {
        extend: {
            colors: {
                'pc-blue': {
                    '900': '#002242',
                    '800': '#00294F',
                    '700': '#00305d',
                    '600': '#004E98',
                    '500': '#006DD3',
                    '400': '#108BFF',
                    '300': '#4CA8FF',
                    '200': '#88C5FF',
                    '100': '#C3E2FF',
                },
                'pc-gold': {
                    '900': '#835c00',
                    '800': '#af7a00',
                    '700': '#db9900',
                    '600': '#ffb507',
                    '500': '#ffc233',
                    '400': '#ffcb50',
                    '300': '#FFD36D',
                    '200': '#FFDC8A',
                    '100': '#FFE5A8',
                },
            }
        },
    },
    variants: {},
    purge: {
        content: [
            './app/**/*.php',
            './resources/**/*.html',
            './resources/**/*.js',
            './resources/**/*.jsx',
            './resources/**/*.ts',
            './resources/**/*.tsx',
            './resources/**/*.php',
            './resources/**/*.vue',
            './resources/**/*.twig',
        ],
        options: {
            defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
            whitelistPatterns: [/-active$/, /-enter$/, /-leave-to$/, /show$/],
        },
    },
    plugins: [
        require('@tailwindcss/ui'),
        require('@tailwindcss/typography'),
    ],
    future: {
        removeDeprecatedGapUtilities: true,
    }
};

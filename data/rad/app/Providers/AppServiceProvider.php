<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Middleware\TrustProxies;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Smorken\SocialAuth\SocialiteProviders\AzureLatestExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;

class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->preventLazyLoadingWhenLocal();
        $this->registerSocialAuthEvents();
        $this->setTrustedProxies();
    }

    public function register(): void
    {
        //
    }

    protected function preventLazyLoadingWhenLocal(): void
    {
        if ($this->app->environment('local')) {
            Model::preventLazyLoading();
        }
    }

    protected function registerSocialAuthEvents(): void
    {
        Event::listen(SocialiteWasCalled::class, AzureLatestExtendSocialite::class);
        //Event::listen(SocialiteWasCalled::class, FakeExtendSocialite::class);
    }

    protected function setTrustedProxies(): void
    {
        TrustProxies::at('*');
    }
}

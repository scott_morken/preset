<?php

declare(strict_types=1);

namespace App\Http\Controllers\Web\Home;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class Controller extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'home';

    public function __invoke(Request $request): View
    {
        return $this->makeView($this->getViewName('index'));
    }
}

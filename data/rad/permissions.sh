#!/usr/bin/env bash

CDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"

USER=
GROUP=www-data

if [ $(id -u) != 0 ]; then
    err "You must run this with sudo or root"
    exit 1
fi

ROGROUP=("app" "bootstrap" "public" "vendor" "config" "resources" "local")
RWGROUP=("storage" "bootstrap/cache")

RODIR=0750
RWDIR=0770
ROFILE=0640
RWFILE=0660

if [ ! -z "$USER" ]; then
    echo "Changing ownership of \"${CDIR}\":     user => \"${USER}\""
    chown -R ${USER} ${CDIR}
fi

echo "Changing ownership of \"${CDIR}/.env\":"
echo "    user => \"${USER}\"    group => \"${GROUP}\""
chown -R ${USER}:${GROUP} "${CDIR}/.env"

echo "Changing permissions of \"${CURR}/.env\" to \"rwxr_x___\""
chmod ${ROFILE} "${CDIR}/.env"

for x in ${ROGROUP[@]}; do
    CURR=${CDIR}/${x}
    if [ ! -d "${CURR}" ]; then
        echo "[${CURR}] does not exist... creating..."
        mkdir -p ${CURR}
    fi
    echo "Changing ownership of \"${CURR}\":"
    echo "    user => \"${USER}\"     group => \"${GROUP}\""
    chown -R ${USER}:${GROUP} ${CURR}

    echo "Changing permissions of directories in \"${CURR}\" to \"rwxr_x___\""
    find ${CURR} -type d -exec chmod ${RODIR} '{}' \;

    echo "Changing permissions of files in \"${CURR}\" to \"rw_r_____\""
    find ${CURR} -type f -exec chmod ${ROFILE} '{}' \;
done

for x in ${RWGROUP[@]}; do
    CURR=${CDIR}/${x}
    if [ ! -d "${CURR}" ]; then
        echo "[${CURR}] does not exist... creating..."
        mkdir -p ${CURR}
    fi
    echo "Changing ownership of \"${CURR}\":"
    echo "    user => \"${USER}\"     group => \"${GROUP}\""
    chown -R ${USER}:${GROUP} ${CURR}

    echo "Changing permissions of directories in \"${CURR}\" to \"rwxrwx___\""
    find ${CURR} -type d -exec chmod ${RWDIR} '{}' \;

    echo "Changing permissions of files in \"${CURR}\" to \"rw_rw____\""
    find ${CURR} -type f -exec chmod ${RWFILE} '{}' \;
done

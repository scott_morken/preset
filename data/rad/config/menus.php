<?php

return [
    'guest' => [],
    'auth' => [],

    'role-manage' => [],
    'role-admin' => [
        [
            'name' => 'Users',
            'action' => [\Smorken\SocialAuth\Http\Controllers\Admin\Controller::class, 'index'],
            'children' => [],
        ],
    ],
];

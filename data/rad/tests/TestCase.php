<?php

namespace Tests\App;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\App\Traits\RefreshApplication;
use Tests\App\Traits\TestingMigrations;
use Tests\App\Traits\WaitForDatabase;

abstract class TestCase extends BaseTestCase
{
    use RefreshApplication, TestingMigrations, WaitForDatabase;
}

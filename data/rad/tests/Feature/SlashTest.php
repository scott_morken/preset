<?php
declare(strict_types=1);

namespace Tests\App\Feature;

it('the root route is a 200 response', function () {
    get('/')->assertStatus(200);
});

<?php

namespace Tests\App\Feature;

use Tests\App\BrowserKitTestCase;

class BrowserKitSlashTest extends BrowserKitTestCase
{
    public function testSlashRoute()
    {
        $this->visit('/')
            ->see('Laravel');
    }
}

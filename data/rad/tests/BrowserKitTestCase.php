<?php

namespace Tests\App;

use Database\Seeders\TestSeeder;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\App\Traits\CreatesApplication;

abstract class BrowserKitTestCase extends \Laravel\BrowserKitTesting\TestCase
{
    use CreatesApplication;

    public $baseUrl = 'http://app.docker';

    protected $shouldSeed = true;

    protected function defineGates()
    {
        if (class_exists(\Smorken\Roles\ServiceProvider::class)) {
            \Smorken\Roles\ServiceProvider::defineGates($this->app, true);
        } elseif (class_exists(\Smorken\SimpleAdmin\ServiceProvider::class)) {
            \Smorken\SimpleAdmin\ServiceProvider::defineGates($this->app, true);
        }
    }

    protected function seedForTest()
    {

    }

    protected function seedTables()
    {
        if ($this->shouldSeed) {
            if (class_exists(TestSeeder::class)) {
                $this->seed(TestSeeder::class);
            }
            if (class_exists(RoleUser::class)) {
                RoleUser::factory()->create(['role_id' => 1, 'user_id' => 1]);
            }
            $this->seedForTest();
        }
    }
}

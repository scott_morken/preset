<?php
declare(strict_types=1);

namespace Tests\App\Traits;

use Illuminate\Contracts\Foundation\Application;

trait RefreshApplication
{

    protected function afterRefreshApplication(Application $app): void
    {
        $uses = array_flip(class_uses_recursive(static::class));

        if (isset($uses[TestingMigrations::class])) {
            $this->addTestingMigrations($app, $this->getTestingMigrations());
        }
        if (isset($uses[WaitForDatabase::class])) {
            $this->waitForDatabaseConnection($app);
        }
    }

    protected function beforeRefreshApplication(): void
    {
        // override
    }

    protected function refreshApplication(): void
    {
        $this->beforeRefreshApplication();
        $this->app = $this->createApplication();
        $this->afterRefreshApplication($this->app);
    }
}

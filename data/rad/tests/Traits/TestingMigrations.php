<?php
declare(strict_types=1);

namespace Tests\App\Traits;

use Illuminate\Contracts\Foundation\Application;

trait TestingMigrations
{

    protected function addTestingMigrations(Application $app, array $paths = []): void
    {
        $reals = array_map(fn($v) => realpath($v), $paths);
        if ($reals) {
            $app->afterResolving('migrator', function ($migrator) use ($reals) {
                foreach ($reals as $real) {
                    $migrator->path($real);
                }
            });
        }
    }

    protected function getTestingMigrations(): array
    {
        $migrationsDir = realpath(__DIR__.'/../database/migrations');
        if ($migrationsDir !== false) {
            return [
                $migrationsDir,
            ];
        }

        return [];
    }
}

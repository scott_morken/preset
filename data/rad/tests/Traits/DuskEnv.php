<?php

namespace Tests\App\Traits;

use Dotenv\Dotenv;
use Symfony\Component\Finder\Finder;

trait DuskEnv
{
    protected static bool $initialized = false;

    protected ?string $basePath = null;

    public function destroy(): void
    {
        $this->teardownDuskEnviroment();
    }

    /**
     * Backup the current environment file.
     */
    protected function backupEnvironment(): void
    {
        copy($this->basePath('.env'), $this->basePath('.env.backup'));

        copy($this->basePath($this->duskFile()), $this->basePath('.env'));
    }

    protected function basePath(?string $path = null): string
    {
        if (is_null($this->basePath)) {
            $this->basePath = realpath(__DIR__.'/../../');
            if (! $this->basePath) {
                throw new \OutOfBoundsException('Path does not exist.');
            }
        }
        if (! is_null($path) && ! str_starts_with($path, DIRECTORY_SEPARATOR)) {
            $path = DIRECTORY_SEPARATOR.$path;
        }

        return $this->basePath.$path;
    }

    /**
     * Get the name of the Dusk file for the environment.
     */
    protected function duskFile(): string
    {
        $duskFiles = ['.env.dusk.local', '.env.dusk.testing', '.env.dusk'];
        foreach ($duskFiles as $duskFile) {
            if (file_exists($this->basePath($duskFile))) {
                return $duskFile;
            }
        }
        throw new \OutOfBoundsException('No dusk file found.');
    }

    protected function initialize(): void
    {
        if (! self::$initialized) {
            $this->purgeScreenshots();
            $this->purgeConsoleLogs();
            $this->purgeSourceLogs();
            $this->setupDuskEnvironment();
            register_shutdown_function(function () {
                $this->destroy();
            });
            self::$initialized = true;
        }
    }

    /**
     * Purge the console logs.
     */
    protected function purgeConsoleLogs(): void
    {
        $this->purgeDebuggingFiles(
            $this->basePath('tests/Browser/console'), '*.log'
        );
    }

    /**
     * Purge debugging files based on path and patterns.
     */
    protected function purgeDebuggingFiles(string $path, string $patterns): void
    {
        if (! is_dir($path)) {
            return;
        }

        $files = Finder::create()->files()
            ->in($path)
            ->name($patterns);

        foreach ($files as $file) {
            @unlink($file->getRealPath());
        }
    }

    /**
     * Purge the failure screenshots.
     */
    protected function purgeScreenshots(): void
    {
        $this->purgeDebuggingFiles(
            $this->basePath('tests/Browser/screenshots'), 'failure-*'
        );
    }

    /**
     * Purge the source logs.
     */
    protected function purgeSourceLogs(): void
    {
        $this->purgeDebuggingFiles(
            $this->basePath('tests/Browser/source'), '*.txt'
        );
    }

    /**
     * Refresh the current environment variables.
     */
    protected function refreshEnvironment(): void
    {
        // BC fix to support Dotenv ^2.2...
        if (! method_exists(Dotenv::class, 'create')) {
            (new Dotenv($this->basePath()))->overload();

            return;
        }

        // BC fix to support Dotenv ^3.0...
        if (! method_exists(Dotenv::class, 'createMutable')) {
            Dotenv::create($this->basePath())->overload();

            return;
        }

        Dotenv::createMutable($this->basePath())->load();
    }

    /**
     * Restore the backed-up environment file.
     */
    protected function restoreEnvironment(): void
    {
        copy($this->basePath('.env.backup'), $this->basePath('.env'));
    }

    /**
     * Setup the Dusk environment.
     */
    protected function setupDuskEnvironment(): void
    {
        if (file_exists($this->basePath($this->duskFile()))) {
            if (file_get_contents($this->basePath('.env')) !== file_get_contents($this->basePath($this->duskFile()))) {
                $this->backupEnvironment();
            }

            $this->refreshEnvironment();
        }
    }

    /**
     * Restore the original environment.
     */
    protected function teardownDuskEnviroment(): void
    {
        if (file_exists($this->basePath($this->duskFile())) && file_exists($this->basePath('.env.backup'))) {
            $this->restoreEnvironment();
        }
    }
}

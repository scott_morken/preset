<?php
declare(strict_types=1);

namespace Tests\App\Traits;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\DB;

trait WaitForDatabase
{

    protected bool $checkDatabase = true;

    protected function waitForDatabaseConnection(Application $app): void
    {
        if (! $this->checkDatabase) {
            return;
        }
        for ($i = 0; $i < 10; $i++) {
            try {
                DB::connection()->getPdo();

                return;
            } catch (\Throwable) {
                sleep(1);
            }
        }
    }
}

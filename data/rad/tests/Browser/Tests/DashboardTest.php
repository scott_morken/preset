<?php
declare(strict_types=1);

namespace Tests\App\Browser\Tests;

use Laravel\Dusk\Browser;
use Smorken\Auth\Models\Eloquent\User;

it('shows a protected route with authentication', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $browser->loginAs($user)
            ->visit('/dashboard')
            ->assertSee('Dashboard')
            ->logout();
    });
});

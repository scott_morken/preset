<?php
declare(strict_types=1);

namespace Tests\Browser\Tests;

use Laravel\Dusk\Browser;

it('shows the login page for unauthenticated users', function () {
    $this->browse(function (Browser $browser) {
        $browser->visit('/')
            ->assertSee('Laravel');
    });
});

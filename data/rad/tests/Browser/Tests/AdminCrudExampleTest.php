<?php
declare(strict_types=1);

namespace Tests\App\Browser\Tests;

use App\Models\Eloquent\MODEL;
use Laravel\Dusk\Browser;
use Smorken\Auth\Models\Eloquent\User;

it('creates a new record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $browser->loginAs($user)
            ->visit('/admin/MODEL')
            ->clickLink('Create')
            ->assertPathIs('/admin/MODEL/create')
            ->type('name', 'Test Create')
            ->check('active')
            ->press('Save')
            ->assertPathIs('/admin/MODEL')
            ->assertSee('Test Create')
            ->logout();
    });
});

it('deletes a record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $model = MODEL::factory()->create();
        $browser->loginAs($user)
            ->visit('/admin/MODEL')
            ->assertSee($model->name)
            ->with('#row-for-'.$model->id, function (Browser $row) {
                $row->clickLink('delete');
            })
            ->assertPathIs('/admin/MODEL/'.$model->id.'/confirm-delete')
            ->assertSee($model->name)
            ->press('Delete')
            ->assertPathIs('/admin/MODEL')
            ->assertDontSee($model->name)
            ->logout();
    });
});

it('fails validation when creating a new record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $browser->loginAs($user)
            ->visit('/admin/MODEL')
            ->clickLink('Create')
            ->press('Save')
            ->assertPathIs('/admin/MODEL/create')
            ->assertSee('name field is required')
            ->logout();
    });
});

it('fails validation when updating a record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $model = MODEL::factory()->create();
        $browser->loginAs($user)
            ->visit('/admin/MODEL/'.$model->id.'/edit')
            ->clear('name')
            ->press('Save')
            ->assertPathIs('/admin/MODEL/'.$model->id.'/edit')
            ->assertSee('name field is required')
            ->logout();
    });
});

it('shows a record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $model = MODEL::factory()->create();
        $browser->loginAs($user)
            ->visit('/admin/MODEL')
            ->assertSee($model->name)
            ->clickLink($model->id)
            ->assertPathIs('/admin/MODEL/'.$model->id)
            ->assertSee($model->name)
            ->logout();
    });
});

it('shows an error for a non admin user', function () {
    $user = User::factory()->create([
        'id' => 2,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $browser->loginAs($user)
            ->visit('/admin/MODEL')
            ->assertSee("You don't have permission")
            ->logout();
    });
});

it('shows an error when deleting a missing record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $browser->loginAs($user)
            ->visit('/admin/MODEL/999/delete')
            ->assertSee('The resource you were trying to reach')
            ->logout();
    });
});

it('shows an error when showing a missing record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $browser->loginAs($user)
            ->visit('/admin/MODEL/999/view')
            ->assertSee('The resource you were trying to reach')
            ->logout();
    });
});

it('shows an error when updating a missing record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $model = MODEL::factory()->create();
        $browser->loginAs($user)
            ->visit('/admin/MODEL/999/edit')
            ->assertSee('The resource you were trying to reach')
            ->logout();
    });
});

it('shows the base route for an admin user', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $browser->loginAs($user)
            ->visit('/admin/MODEL')
            ->assertSee('Types Administration')
            ->assertSee('Web Application')
            ->logout();
    });
});

it('updates an existing record', function () {
    $user = User::factory()->create([
        'id' => 1,
        'username' => 'foobar',
        'email' => 'foobar@example.org',
        'first_name' => 'foo',
        'last_name' => 'bar',
    ]);
    $this->browse(function (Browser $browser) use ($user) {
        $model = MODEL::factory()->create();
        $browser->loginAs($user)
            ->visit('/admin/MODEL')
            ->with('#row-for-'.$model->id, function (Browser $row) {
                $row->clickLink('edit');
            })
            ->assertPathIs('/admin/MODEL/'.$model->id.'/edit')
            ->assertInputValue('name', $model->name)
            ->assertChecked('active')
            ->type('name', 'Test Update')
            ->press('Save')
            ->assertPathIs('/admin/MODEL')
            ->assertSee('Test Update')
            ->logout();
    });
});

<?php

namespace Tests\App;

use Database\Seeders\TestSeeder;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\TestCase as BaseTestCase;
use PHPUnit\Framework\Attributes\BeforeClass;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\App\Traits\DuskBrowser;
use Tests\App\Traits\DuskEnv;
use Tests\App\Traits\RefreshApplication;
use Tests\App\Traits\TestingMigrations;
use Tests\App\Traits\WaitForDatabase;

abstract class DuskTestCase extends BaseTestCase
{
    use RefreshApplication, DatabaseTruncation, DuskBrowser, DuskEnv, TestingMigrations, WaitForDatabase;

    protected string $seeder = TestSeeder::class;

    #[BeforeClass]
    public static function prepare(): void
    {
        if (! static::runningInSail()) {
            // static::startChromeDriver();
        }
    }

    protected function addRoleForUserId(int $userId = 1, int $roleId = 1): void
    {
        if (class_exists(RoleUser::class)) {
            RoleUser::factory()->create(['user_id' => $userId, 'role_id' => $roleId]);
        }
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return RemoteWebDriver
     *
     * @throws \Exception
     */
    protected function driver(): RemoteWebDriver
    {
        $this->checkEndpoints($this->getUrls());

        return $this->getDriver();
    }

    protected function setUp(): void
    {
        $this->initialize();
        parent::setUp();
        $this->addRoleForUserId();
    }
}

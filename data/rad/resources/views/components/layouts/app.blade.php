@props(['isFluid' => false, 'title' => null, 'homeUrl' => url('/')])
<x-layouts.base :title="$title">
    <div id="ajax-loading" class="spinner-border text-warning" role="status"><span
                class="visually-hidden">Loading...</span></div>
    <header>
        @if (isset($header) && !$header->isEmpty())
            {{ $header }}
        @else
            <x-smc::default.header :container="$isFluid ? 'container-fluid' : 'container'" :homeUrl="$homeUrl"></x-smc::default.header>
        @endif
    </header>
    <main {{ $attributes->class(['py-4', ($isFluid ? 'container-fluid' : 'container')]) }}>
        {{ $slot }}
    </main>
    <footer {{ $attributes->class(['py-4', ($isFluid ? 'container-fluid' : 'container')])->only(['class']) }}>
        @if (isset($footer) && !$footer->isEmpty())
            {{ $footer }}
        @else
            <x-smc::default.footer></x-smc::default.footer>
        @endif
    </footer>
</x-layouts.base>

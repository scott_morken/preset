@props(['title' => null])
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}" sizes="16x16 32x32 48x48 64x64"
          type="image/vnd.microsoft.icon">
    @section('scripts_head')
        <!-- Scripts -->
    @show
    @section('styles')
        <!-- Styles -->
        <x-frontend-assets-bootstrap-styles />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @show
    @stack('add_to_head')
</head>
<body>
<div id="app">
    {{ $slot }}
</div>
<x-frontend-assets-bootstrap-scripts />
<script src="{{ asset('js/app.js') }}"></script>
@stack('add_to_end')
</body>
</html>

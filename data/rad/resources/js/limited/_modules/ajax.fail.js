const ajaxFail = {
    fail: function (data) {
        const response = data.response || {};
        const status = parseInt(response.status);
        if (status === 404 || status === 422) {
            return;
        }
        if (status > 299 && status < 500) {
            window.location.reload();
        } else {
            let message = 'There was an error connecting to the backend.';
            alert(message);
            console.log(data);
        }
    }
};

export default ajaxFail;

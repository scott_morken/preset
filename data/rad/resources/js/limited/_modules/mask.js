export class Mask {
    constructor({
                    toggleId = 'toggle-mask',
                    unmaskedClasses = ['text-success', 'bi-eye-fill'],
                    maskedClasses = ['text-danger', 'bi-eye-slash-fill', 'masked'],
                    hasMaskClass = 'masked',
                    canBlurSelector = '.can-blur',
                    blurClasses = ['blur']
                } = {}) {
        this.toggleElement = document.getElementById(toggleId);
        this.unmaskedClasses = unmaskedClasses;
        this.maskedClasses = maskedClasses;
        this.hasMaskClass = hasMaskClass;
        this.canBlurSelector = canBlurSelector;
        this.blurClasses = blurClasses;
        this.initEvents();
        this.initIconClasses();
    }

    initEvents() {
        const that = this;
        this.toggleElement.addEventListener('click', function (event) {
            that.toggle();
        });
    }

    initIconClasses() {
        const icon = this.getIconElement();
        if (this.hasBlurredElements()) {
            this.iconMaskClasses(icon);
        } else {
            this.iconUnmaskClasses(icon);
        }
    }

    toggle() {
        const icon = this.getIconElement();
        if (this.isMasked(icon)) {
            this.unmask(icon);
        } else {
            this.mask(icon);
        }
    }

    mask(icon) {
        const that = this;
        this.getBlurrableElements().forEach(function (element) {
            element.classList.add(...that.blurClasses);
        });
        this.iconMaskClasses(icon);
    }

    iconMaskClasses(icon) {
        icon?.classList.add(...this.maskedClasses);
        icon?.classList.remove(...this.unmaskedClasses);
    }

    unmask(icon) {
        const that = this;
        this.getBlurrableElements().forEach(function (element) {
            element.classList.remove(...that.blurClasses);
        });
        this.iconUnmaskClasses(icon);
    }

    iconUnmaskClasses(icon) {
        icon?.classList.add(...this.unmaskedClasses);
        icon?.classList.remove(...this.maskedClasses);
    }

    getBlurrableElements() {
        return document.querySelectorAll(this.canBlurSelector);
    }

    hasBlurredElements() {
        const that = this;
        for (const element of this.getBlurrableElements()) {
            if (element.classList.contains(that.blurClasses[0])) {
                return true;
            }
        }
        return false;
    }

    getIconElement() {
        return this.toggleElement.getElementsByTagName('i')[0];
    }

    isMasked(element) {
        return element?.classList.contains(this.hasMaskClass);
    }
}

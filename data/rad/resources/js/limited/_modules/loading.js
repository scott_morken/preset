import {SpinnerTracker} from "./spinner.tracker";

export class Loading {
    constructor(loading_id, divX = 2, divY = 4) {
        this.loading_id = loading_id;
        this.divX = divX;
        this.divY = divY;
        this.getElement().style.position = 'absolute';
        this.getElement().style.zIndex = '999';
    }

    start() {
        const el = this.getElement();
        if (this.divX) {
            el.style.left = this.getXLocationInViewport(el) + 'px';
        }
        if (this.divY) {
            el.style.top = this.getYLocationInViewport(el) + 'px';
        }
        el.style.display = 'block';
        SpinnerTracker.enable();
    }

    stop() {
        SpinnerTracker.disable();
        if (!SpinnerTracker.enabled) {
            this.getElement().style.display = 'none';
        }
    }

    getElement() {
        return document.getElementById(this.loading_id);
    }

    getXLocationInViewport(element) {
        const elementWidth = element.offsetWidth;
        const viewportWidth = window.innerWidth || document.documentElement.clientWidth;
        const scrollX = window.pageXOffset;
        const addToX = (viewportWidth + elementWidth) / this.divX;
        return Math.round(scrollX + addToX);
    }

    getYLocationInViewport(element) {
        const elementHeight = element.offsetHeight;
        const viewportHeight = window.innerHeight || document.documentElement.clientHeight;
        const scrollY = window.pageYOffset;
        const addToY = (viewportHeight - elementHeight) / this.divY;
        return Math.round(scrollY + addToY);
    }
}

import {SimpleAlpine} from "./simple.alpine";
import {TabsEvents} from "../constants/tabs.events";

export class Tabs extends SimpleAlpine {


    select(tabId) {
        this.alpineDispatch(TabsEvents.TABS_SELECT, {fromId: this.activeTab, toId: tabId});
        this.activeTab = tabId;
    }

    isActive(tabId) {
        return this.activeTab === tabId;
    }

    tabBinds(tabId) {
        const that = this;
        return {
            ['x-on:click.prevent']() {
                that.select(tabId)
            },
            ['x-bind:id']() {
                return `tab-${tabId}`
            },
            ['x-bind:class']() {
                return {'active': that.isActive(tabId)}
            },
            ['x-bind:aria-controls']() {
                return `tab-panel-${tabId}`
            },
            ['x-bind:aria-selected']() {
                return that.isActive(tabId)
            }
        }
    }

    panelBinds(tabId) {
        const that = this;
        return {
            ['x-bind:id']() {
                return `tab-panel-${tabId}`
            },
            ['x-bind:class']() {
                return {'active': that.isActive(tabId)}
            },
            ['x-bind:aria-labelledby']() {
                return `tab-${tabId}`
            },
            ['x-bind:tabindex']() {
                return that.isActive(tabId) ? 1 : 0;
            }
        }
    }
}

import {SimpleAlpine} from "./simple.alpine";
import {CollapseEvents} from "../constants/collapse.events";

export class CollapseToggler extends SimpleAlpine {

    show() {
        this.alpineDispatch(CollapseEvents.SHOW);
    }

    hide() {
        this.alpineDispatch(CollapseEvents.HIDE);
    }
}

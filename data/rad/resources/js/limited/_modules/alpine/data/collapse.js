import {Collapse} from "./classes/collapse";

export default ({
                    hidden = true,
                    name = 'detail'
                } = {}) => ({
    Collapse: new Collapse({
        hidden: hidden,
        name: name
    }),
    init() {
        this.Collapse.init(this);
    }
});

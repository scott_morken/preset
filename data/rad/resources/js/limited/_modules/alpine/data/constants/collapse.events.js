export class CollapseEvents {

    static HIDE = 'collapse-hide';

    static SHOW = 'collapse-show';
}

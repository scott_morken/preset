import {SimpleAlpine} from "./simple.alpine";

export class KeyObject {

    constructor(parent, parentKey) {
        this.parent = parent;
        this.parentKey = parentKey;
        if (!this.parent[this.parentKey]) {
            this.parent[this.parentKey] = {};
        }
    }

    add(key) {
        if (key && !this.items[key]) {
            this.items[key] = {};
        }
    }

    remove(key) {
        delete this.items[key];
    }

    addChild(parentKey, childKey) {
        this.add(parentKey);
        if (childKey && !this.items[parentKey][childKey]) {
            this.items[parentKey][childKey] = null;
        }
    }

    removeChild(parentKey, childKey) {
        delete this.items[parentKey][childKey];
    }

    get items() {
        return this.parent[this.parentKey];
    }
}

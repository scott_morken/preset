import {SimpleAlpine} from "./simple.alpine";
import {CollapseEvents} from "../constants/collapse.events";

export class Collapse extends SimpleAlpine {

    initAfterAlpine() {
        this.id = this.next(this.name, this.name + '-');
        this.addEventListener(CollapseEvents.HIDE, () => this.hidden = true);
        this.addEventListener(CollapseEvents.SHOW, () => this.hidden = false);
    }

    bindTrigger() {
        const that = this;
        return {
            ['x-text']() {
                return that.text;
            }, ['x-on:click.prevent']() {
                that.toggle();
            }, ['x-bind:aria-expanded']: '!Collapse.isHidden()',
            ['x-bind:aria-controls']() {
                return that.id;
            },
            ['x-bind:class']() {
                return 'mb-2';
            }
        }
    }

    bindContainer() {
        const that = this;
        return {
            ['x-show']: '!Collapse.isHidden()',
            ['x-bind:id']() {
                return that.id;
            }
        }
    }

    isHidden() {
        return this.hidden;
    }

    toggle() {
        this.hidden = !this.hidden;
    }

    get text() {
        return this.isHidden() ? 'Show ' + this.name : 'Hide ' + this.name;
    }
}

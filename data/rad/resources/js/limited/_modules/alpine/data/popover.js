export default () => ({
    popover: {
        element: null
    },
    init() {
        this.popover.element = new bootstrap.Popover(this.$root);
    }
});

import {SimpleAlpine} from "./simple.alpine";

export class Paginator extends SimpleAlpine {


    get currentPage() {
        return this.getMeta().current_page ?? null;
    }

    get total() {
        return this.getMeta().total ?? 0;
    }

    get from() {
        return this.getMeta().from ?? 0;
    }

    get to() {
        return this.getMeta().to ?? 0;
    }

    get links() {
        return this.dataProvider().links ?? [];
    }

    page(link) {
        this.dataProvider().search(this.getPageFromLink(link));
    }

    getPageFromLink(link) {
        const url = new URL(link.url);
        return url.searchParams.get('page') ?? 1;
    }

    show() {
        return this.links.length > 0;
    }

    dataProvider() {
        return this.provider;
    }

    getMeta() {
        return this.dataProvider().meta ?? {};
    }

    getLiClasses(link) {
        const classes = [];
        if (!(link.url ?? null)) {
            classes.push('disabled');
        }
        if ((link.active ?? false)) {
            classes.push('active');
        }
        return classes.join(' ');
    }

    getLinkUrl(link) {
        return link.url ?? '';
    }

    getLinkText(link) {
        return link.label;
    }

    getAriaLabel(link) {
        return this.getLinkText(link);
    }
}

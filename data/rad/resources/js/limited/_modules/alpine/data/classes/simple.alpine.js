import {Unique} from "../../../unique";
import {SafeCssId} from "../../../safe.css.id";
import {GetSetData} from "../../../helpers/get.set.data";
import {LogHelper} from "../../../helpers/log.helper";

export class SimpleAlpine {

    constructor(data) {
        this.alpine = null;
        this.unique = Unique;
        this.safeCssId = SafeCssId;
        this.setData(this.applyDefaults(data));
        this.onInstantiate();
    }

    onInstantiate() {
    }

    setAlpine(alpine) {
        this.alpine = alpine;
    }

    init(alpine) {
        this.setAlpine(alpine);
        this.initAfterAlpine(...[...arguments].slice(1));
    }

    initAfterAlpine() {

    }

    setData(data) {
        for (const [k, v] of Object.entries(data)) {
            this[k] = v;
        }
    }

    applyDefaults(data) {
        for (const [key, value] of Object.entries(this.getDefaults())) {
            if (data[key] === undefined || data[key] === null) {
                data[key] = value;
            }
        }
        return data;
    }

    addEventListener(eventName, callback, element = null) {
        element = element ?? document;
        element.addEventListener(eventName, callback);
    }

    alpineDispatch(eventName, detail) {
        this.getAlpine().$dispatch(eventName, detail);
    }

    alpineFocus() {
        return this.getAlpine().$focus;
    }

    alpineNextTick(callback) {
        this.getAlpine().$nextTick(callback);
    }

    alpineWatch(key, callback) {
        this.getAlpine().$watch(key, callback);
    }

    getAlpine() {
        return this.alpine;
    }

    getAlpineId(key) {
        return this.getAlpine().$id(key);
    }

    getAlpineData(key, defaultValue = null) {
        return this.getAlpine().$data[key] ?? defaultValue;
    }

    getAlpineElement() {
        return this.getAlpine().$el;
    }

    getAlpineRoot() {
        return this.getAlpine().$root;
    }

    getDefaults() {
        return {};
    }

    isEmptyIndex(value) {
        return this.isEmpty(value) || value === '0';
    }

    isEmptyValue(value) {
        return this.isEmpty(value) || value === '';
    }

    isEmpty(value) {
        return value === undefined || value === null || typeof value === 'undefined';
    }

    log() {
        console.log(this.constructor.name, this.toJson());
    }

    next(key, prefix = undefined) {
        return this.unique.next(key, prefix);
    }

    notEmpty(value) {
        return !this.isEmpty(value);
    }

    safe(value) {
        return this.safeCssId.get(value);
    }

    toBoolean(key) {
        return Boolean(this[key]);
    }

    toJson() {
        return LogHelper.jsonify(this);
    }
}

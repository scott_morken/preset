import {Tabs} from "./classes/tabs";

export default ({
                    tabs,
                    activeTab = 0
                } = {}) => ({
    Tabs: new Tabs({activeTab: activeTab, tabs: tabs}),
    init() {
        this.Tabs.init(this);
    }
});

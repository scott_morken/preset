import {KeyObject} from "./classes/key.object";

export default (parent, parentKey = 'htmlAttributes') => ({
    KeyObject: new KeyObject(parent, parentKey)
});

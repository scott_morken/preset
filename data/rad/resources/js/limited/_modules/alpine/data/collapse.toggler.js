import {CollapseToggler} from "./classes/collapse.toggler";

export default () => ({
    CollapseToggler: new CollapseToggler({}),
    init() {
        this.CollapseToggler.init(this);
    }
});

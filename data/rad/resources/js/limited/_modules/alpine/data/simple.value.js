import {SimpleValue} from "./classes/simple.value";

export default (value = null) => ({
    SimpleValue: new SimpleValue({value: value}),
    init() {
        this.SimpleValue.init(this);
    }
});

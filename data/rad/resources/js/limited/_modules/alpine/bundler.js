import popover from "./data/popover";
import {BaseBundler} from "./base.bundler";
import collapse from "./data/collapse";
import keyObject from "./data/key.object";
import simpleValue from "./data/simple.value";
import tabs from "./data/tabs";
import collapseToggler from "./data/collapse.toggler";

export class Bundler extends BaseBundler {

    data() {
        return {
            collapse: collapse,
            collapseToggler: collapseToggler,
            keyObject: keyObject,
            popover: popover,
            simpleValue: simpleValue,
            tabs: tabs
        };
    }
}

export class BaseBundler {

    register(Alpine) {
        for (const [key, func] of this.iterable(this.data())) {
            Alpine.data(key, func);
        }
    }

    data() {
        return {};
    }

    iterable(obj) {
        return Object.entries(obj);
    }
}

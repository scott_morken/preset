export class CacheHelper {

    static cache = {};

    constructor(timeout = 120000) {
        this.timeout = timeout;
    }

    remove(key) {
        delete CacheHelper.cache[key];
    }

    has(key) {
        if (this.keyExists(key)) {
            if (this.lessThanTimeout(key)) {
                return true;
            }
            this.remove(key);
        }
        return false;
    }

    differenceToNow(key) {
        const current = new Date().getTime();
        const cached = CacheHelper.cache[key]._;
        return (current - cached);
    }

    lessThanTimeout(key) {
        return this.differenceToNow(key) < this.timeout;
    }

    keyExists(key) {
        return Boolean(CacheHelper.cache[key] ?? false);
    }

    to(key, value) {
        CacheHelper.cache[key] = {
            _: new Date().getTime(),
            data: value
        };
    }

    from(key) {
        if (this.has(key)) {
            return CacheHelper.cache[key]?.data ?? null;
        }
        return null;
    }
}

export class GetSetData {

    constructor(that, key = 'data') {
        this.boot(that, key);
    }

    boot(that, key) {
        this.ensureKeyOnObject(that, key);
        const keys = Object.keys(that[key]);
        for (const attribute of keys) {
            this.defineGetterAndSetter(that, key, attribute);
        }
    }

    ensureKeyOnObject(that, key) {
        const exists = that[key];
        if (this.isValueNullOrUndefined(exists)) {
            that[key] = {};
        }
    }

    defineGetterAndSetter(that, key, attribute) {
        const props = {};
        this.defineGetter(that, key, attribute, props);
        this.defineSetter(that, key, attribute, props);
        if (!this.isObjectEmpty(props)) {
            Object.defineProperty(that, attribute, props);
        }
    }

    defineGetter(that, key, attribute, props) {
        if (!this.hasGetter(that, attribute)) {
            props.get = () => {
                return that[key][attribute] ?? undefined;
            }
        } else {
            props.get = this.getPropertyDescriptor(that, attribute).get;
        }
    }

    defineSetter(that, key, attribute, props) {
        if (!this.hasSetter(that, attribute)) {
            props.set = (value) => {
                that[key][attribute] = value;
            }
        } else {
            props.set = this.getPropertyDescriptor(that, attribute).set;
        }
    }

    getPropertyDescriptor(that, key) {
        let desc;
        do {
            desc = Object.getOwnPropertyDescriptor(that, key);
        } while (!desc && (that = Object.getPrototypeOf(that)));
        return desc;
    }

    hasGetter(that, key) {
        return !this.isValueNullOrUndefined(this.getPropertyDescriptor(that, key)?.get);
    }

    hasSetter(that, key) {
        return !this.isValueNullOrUndefined(this.getPropertyDescriptor(that, key)?.set);
    }

    isObjectEmpty(value) {
        return Object.keys(value).length === 0;
    }

    isValueNullOrUndefined(value) {
        if (value === null) {
            return true;
        }
        if (value === undefined) {
            return true;
        }
        return typeof value === 'undefined';
    }
}

export class DateHelper {

    toDate(value, removeHours) {
        removeHours = removeHours ?? true;
        const date = new Date(value);
        date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
        if (removeHours) {
            date.setHours(0, 0, 0, 0);
        }
        return date;
    }

    format(value) {
        const date = this.toDate(value, true);
        return new Intl.DateTimeFormat(undefined, {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
        }).format(date);
    }
}

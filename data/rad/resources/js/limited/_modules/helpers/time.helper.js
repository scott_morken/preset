export class TimeHelper {

    /**
     *
     * @param {Date|string|null} time
     * @returns {string}
     */
    format(time) {
        if (!time) {
            return '';
        }
        if (!(time instanceof Date)) {
            time = this.toDate(time);
        }
        return new Intl.DateTimeFormat(undefined, {
            hour12: true,
            hour: 'numeric',
            minute: 'numeric'
        }).format(time);
    }

    toDate(time) {
        if (time instanceof Date) {
            return time;
        }
        const date = new Date();
        const hm = this.getHoursAndMinutes(time);
        date.setHours(hm.hours);
        date.setMinutes(hm.minutes);
        date.setSeconds(0);
        return date;
    }

    /**
     * @param {null|string} timeString
     * @returns {{hours: number, minutes: number}}
     */
    getHoursAndMinutes(timeString) {
        if (!timeString) {
            return {hours: 0, minutes: 0};
        }
        const parts = timeString.split(/:| /);
        let hours = parseInt(parts[0]);
        const minutes = parseInt(parts[1]);
        if (parts[2]?.toLowerCase() === 'pm' && hours < 12) {
            hours += 12;
        }
        if (parts[2]?.toLowerCase() === 'am' && hours === 12) {
            hours = 0;
        }
        return {
            hours: hours,
            minutes: minutes
        };
    }
}

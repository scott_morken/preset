export class LogHelper {

    static jsonify(data) {
        return JSON.parse(JSON.stringify(data));
    }
}

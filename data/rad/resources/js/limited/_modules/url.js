const getUrl = (id) => {
    return document.getElementById(id).getAttribute('href');
};

export default getUrl;

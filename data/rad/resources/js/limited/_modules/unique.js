export class Unique {

    static counts = {};

    static next(key, prefix = 'new') {
        key = key ?? 'none';
        if (Unique.counts[key] === undefined) {
            Unique.reset(key);
        }
        Unique.counts[key]++;
        return `${prefix}${Unique.counts[key]}`;
    }

    static reset(key) {
        if (key) {
            Unique.counts[key] = 0;
        } else {
            Unique.counts = {};
        }
    }
}

import ajaxFail from "./ajax.fail";
import {Loading} from "./loading";

export class AxiosBase {

    constructor({
                    loadingId = 'ajax-loading',
                    triggerFail = true,
                } = {}) {
        this.loading = loadingId ? new Loading(loadingId) : null;
        this.triggerFail = triggerFail;
    }

    doGet(url, success, failure, final, config) {
        this.loading?.start();
        axios.get(url, config)
            .then((response) => {
                this.success(response, success);
            })
            .catch((error) => {
                this.failure(error, failure);
            })
            .finally(() => {
                this.finally(final);
            });
    }

    doPost(url, data, success, failure, final) {
        this.loading?.start();
        axios.post(url, data)
            .then((response) => {
                this.success(response, success);
            })
            .catch((error) => {
                this.failure(error, failure);
            })
            .finally(() => {
                this.finally(final);
            });
    }

    success(response, callback) {
        callback(response);
    }

    failure(error, callback) {
        if (typeof callback === 'function') {
            callback(error);
        }
        if (this.triggerFail) {
            ajaxFail.fail(error);
        }
    }

    finally(callback) {
        this.loading?.stop();
        if (typeof callback === 'function') {
            callback();
        }
    }

    sleep(milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
            currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    }


    getUrl(id) {
        return document.getElementById(id).getAttribute('href');
    }
}

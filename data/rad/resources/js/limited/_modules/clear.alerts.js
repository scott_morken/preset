export class ClearAlerts {
    constructor({
                    selector = 'div.clear-alert',
                    timeout = 15000
                } = {}) {
        this.selector = selector;
        setTimeout(() => this.clear(), timeout);
    }

    clear() {
        for (const element of this.getAlertElements()) {
            element.remove();
        }
    }

    getAlertElements() {
        return document.querySelectorAll(this.selector);
    }
}

export class SpinnerTracker {

    static counter = 0;
    static enabled = false;

    static enable() {
        SpinnerTracker.counter++;
        SpinnerTracker.enabled = true;
    }

    static disable() {
        if (SpinnerTracker.counter > 0) {
            SpinnerTracker.counter--;
        }
        if (SpinnerTracker.counter === 0) {
            SpinnerTracker.enabled = false;
        }
    }
}

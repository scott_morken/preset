export class SafeCssId {

    static get(str) {
        return encodeURIComponent(str).toLowerCase().replace(/\.|%[0-9a-z]{2}/gi, '');
    }
}

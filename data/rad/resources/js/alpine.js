import Alpine from 'alpinejs';

window.Alpine = Alpine;

const app = {
    wantsAlpine: 0,
    initializedAlpine: 0
};

document.addEventListener('wants-alpine', () => {
    app.wantsAlpine++;
});

document.addEventListener('alpine-initialized', () => {
    app.initializedAlpine++;
    if (app.initializedAlpine === app.wantsAlpine) {
        Alpine.start();
    }
});

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Smorken\Model\Contracts\Model;

abstract class ModelSeeder extends Seeder
{
    protected mixed $model = null;

    protected string $modelClass;

    abstract protected function getData(): array;

    public function run(): void
    {
        $this->truncate();
        foreach ($this->getData() as $row) {
            $this->create($row);
        }
    }

    protected function create(array $attributes): Model
    {
        return $this->getModel()
            ->create($attributes);
    }

    protected function getModel(): Model
    {
        if (! $this->model) {
            $this->model = $this->container->make($this->modelClass);
        }

        return $this->model;
    }

    protected function truncate(): mixed
    {
        return $this->getModel()
            ->truncate();
    }
}
